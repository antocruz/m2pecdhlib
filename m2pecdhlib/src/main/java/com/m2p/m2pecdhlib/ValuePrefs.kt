package com.m2p.m2pecdhlib

import android.content.Context
import android.content.SharedPreferences

open class ValuePrefs(var context: Context) {
    val primarykey = "Primary_key"
    val exposeKey = "Exposed_key"

    private val prefs: SharedPreferences by lazy {
        context.getSharedPreferences(javaClass.simpleName, Context.MODE_PRIVATE)
    }

    fun setValues(primaryKey:String,exposedKey:String){
        with (prefs.edit()) {
            putString(primarykey,primaryKey)
            putString(exposeKey,exposedKey)
            apply()
        }
    }

    fun getPrimaryKey():String?{
        return prefs.getString(primarykey,"")
    }

    fun getExposedKey():String?{
        return prefs.getString(exposeKey,"")
    }

    fun isHasValues():Boolean {
        return prefs.getString(exposeKey,"")?.length!! > 0
    }


}