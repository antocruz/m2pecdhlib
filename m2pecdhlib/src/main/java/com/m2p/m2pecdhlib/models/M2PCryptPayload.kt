package com.m2p.m2pecdhlib.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class M2PCryptPayload(
    @SerializedName("encryptedReq")
    @Expose
    val encryptedReq: String? = null,
    @SerializedName("encryptedRes")
    @Expose
    val encryptedRes: String? = null
)