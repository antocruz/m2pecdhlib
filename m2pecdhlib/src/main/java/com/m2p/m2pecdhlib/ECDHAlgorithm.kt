package com.m2p.m2pecdhlib

import android.content.Context
import android.util.Base64
import android.util.Log
import org.spongycastle.asn1.ASN1Sequence
import org.spongycastle.asn1.DERBitString
import org.spongycastle.asn1.DERSequence
import org.spongycastle.jce.ECNamedCurveTable
import org.spongycastle.jce.provider.BouncyCastleProvider
import org.spongycastle.jce.spec.ECPublicKeySpec
import org.spongycastle.math.ec.ECPoint
import java.io.ByteArrayOutputStream
import java.security.*
import java.security.KeyPairGenerator
import java.security.Security
import java.security.spec.ECGenParameterSpec
import java.security.spec.PKCS8EncodedKeySpec
import javax.crypto.*
import javax.crypto.spec.IvParameterSpec
import kotlin.experimental.and

internal class ECDHAlgorithm {

    private val algorithmECDH = "ECDH"
    private val primeCurve = "prime256v1"
    private val algorithmAES = "AES/CBC/PKCS7Padding"


    // generates public key and converts and saves the private key in shared prefs
     fun generateKeys(context: Context): String {

        Security.insertProviderAt(BouncyCastleProvider(), 1)

        val ecParamSpec = ECGenParameterSpec(primeCurve)
        val kpg = KeyPairGenerator.getInstance(algorithmECDH)
        kpg.initialize(ecParamSpec)
        val keyPair = kpg.generateKeyPair()

        val privatekey = keyPair?.private?.encoded?.let {encodeHexString(it) }

        val sequencePublic: ASN1Sequence = DERSequence.getInstance(keyPair?.public?.encoded)

        val subjectPublicKey: DERBitString = sequencePublic.getObjectAt(1) as DERBitString

        val subjectPublicKeyBytes: ByteArray = subjectPublicKey.bytes

        val publickey = encodeHexString(subjectPublicKeyBytes)

        if (privatekey != null) {
            ValuePrefs(context).setValues(privatekey,publickey)
        }
        return encodeHexString(subjectPublicKeyBytes)
    }

    // creates the secret using the server hex string and shared pref private key
    fun decryptData(serverPublicKey: String, encrypted: String, context: Context): String {

        val kf = KeyFactory.getInstance(algorithmECDH)
        val privateKey1 = kf.generatePrivate(
            PKCS8EncodedKeySpec(
                ValuePrefs(context).getPrimaryKey()?.let {
                    decodeHexString(it)
                }
            )
        )

        val ecSpec = ECNamedCurveTable.getParameterSpec(primeCurve)
        val point: ECPoint = ecSpec.curve.decodePoint(decodeHexString(serverPublicKey))
        val pubSpec = ECPublicKeySpec(point, ecSpec)
        val publicKey = kf.generatePublic(pubSpec)

        val keyAgreement = KeyAgreement.getInstance(algorithmECDH)
        keyAgreement.init(privateKey1)
        keyAgreement.doPhase(publicKey, true)

        val key: SecretKey = keyAgreement.generateSecret(algorithmECDH)

        return decrypt(encrypted, key)
    }

    fun encryptData(context: Context,serverPublicKey: String, value: String): String {
        Security.insertProviderAt(BouncyCastleProvider(), 1)

        val kf = KeyFactory.getInstance(algorithmECDH)

        val privateKey1 = kf.generatePrivate(
            PKCS8EncodedKeySpec(
                ValuePrefs(context).getPrimaryKey()?.let {
                    decodeHexString(it)
                }
            )
        )

        val ecSpec = ECNamedCurveTable.getParameterSpec(primeCurve)
        val point: ECPoint = ecSpec.curve.decodePoint(decodeHexString(serverPublicKey))
        val pubSpec = ECPublicKeySpec(point, ecSpec)
        val publicKey = kf.generatePublic(pubSpec)


        val keyAgreement = KeyAgreement.getInstance(algorithmECDH)
        keyAgreement.init(privateKey1)
        keyAgreement.doPhase(publicKey, true)

        val key: SecretKey = keyAgreement.generateSecret(algorithmECDH)
        return getEncryptData(key, value)
    }

    // Decrypts the encrypted json string
    @Throws(java.lang.Exception::class)
    fun decrypt(encrypted: String, key: SecretKey): String {
        val iv = ByteArray(16)
        val ivSpec = IvParameterSpec(iv)
        val cipher = Cipher.getInstance(algorithmAES)

        cipher.init(Cipher.DECRYPT_MODE, key, ivSpec)
        return String(
            cipher.doFinal(
                Base64.decode(
                    encrypted,
                    Base64.DEFAULT
                )
            )
        )
    }

    //To convert byte array to hex string
    private fun encodeHexString(byteArray: ByteArray): String {
        val hexStringBuffer = StringBuffer()
        for (i in byteArray.indices) {
            hexStringBuffer.append(byteToHex(byteArray[i].toInt()))
        }
        return hexStringBuffer.toString()
    }

    // To convert hex string to byte array
    private fun decodeHexString(hexString: String): ByteArray {
        require(hexString.length % 2 != 1) { "Invalid hexadecimal String supplied." }
        val bytes = ByteArray(hexString.length / 2)
        var i = 0
        while (i < hexString.length) {
            bytes[i / 2] = hexToByte(hexString.substring(i, i + 2))
            i += 2
        }
        return bytes
    }


    // Byte to hex convertor
    private fun byteToHex(num: Int): String {
        val hexDigits = CharArray(2)
        hexDigits[0] = Character.forDigit(num shr 4 and 0xF, 16)
        hexDigits[1] = Character.forDigit(num and 0xF, 16)
        return String(hexDigits)
    }

    // Hex to byte convertor
    private fun hexToByte(hexString: String): Byte {
        val firstDigit = toDigit(hexString[0])
        val secondDigit = toDigit(hexString[1])
        return ((firstDigit shl 4) + secondDigit).toByte()
    }

    // Digit conversion
    private fun toDigit(hexChar: Char): Int {
        val digit = Character.digit(hexChar, 16)
        require(digit != -1) { "Invalid Hexadecimal Character: $hexChar" }
        return digit
    }

    fun getEncryptData(secretKey: SecretKey, param: String): String {
        val iv = ByteArray(16)
        val cipher = Cipher.getInstance("AES/CBC/PKCS7Padding")
        //val keySpec = SecretKeySpec(secretKey.toByteArray(Charsets.UTF_8), "AES")
        val ivSpec = IvParameterSpec(iv)
        cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivSpec)
        val resultData = cipher.doFinal(param.toByteArray(Charsets.UTF_8))
        return resultData.encodeBase64ToString()
    }

    fun ByteArray.encodeBase64(): ByteArray {
        val table =
            (CharRange('A', 'Z') + CharRange('a', 'z') + CharRange('0', '9') + '+' + '/').toCharArray()
        val output = ByteArrayOutputStream()
        var padding = 0
        var position = 0
        while (position < this.size) {
            var b = this[position].toInt() and 0xFF shl 16 and 0xFFFFFF
            if (position + 1 < this.size) b =
                b or (this[position + 1].toInt() and 0xFF shl 8) else padding++
            if (position + 2 < this.size) b = b or (this[position + 2].toInt() and 0xFF) else padding++
            for (i in 0 until 4 - padding) {
                val c = b and 0xFC0000 shr 18
                output.write(table[c].code)
                b = b shl 6
            }
            position += 3
        }
        for (i in 0 until padding) {
            output.write('='.code)
        }
        return output.toByteArray()
    }

    fun ByteArray.encodeBase64ToString(): String = String(this.encodeBase64())


}