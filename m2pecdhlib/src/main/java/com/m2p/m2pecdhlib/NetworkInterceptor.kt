package com.m2p.m2pecdhlib


import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.m2p.m2pecdhlib.models.M2PCryptPayload
import okhttp3.*
import okhttp3.Headers.Companion.headersOf
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.ResponseBody.Companion.toResponseBody
import okio.Buffer
import java.io.IOException
import java.net.SocketTimeoutException
import java.util.*


open class NetworkInterceptor(context: Context, headers: Headers, serverPublicKey : String) :
    Interceptor {

    private val RESPONSE_UNAUTHORIZED_401 = 401
    var context: Context? = null
    var headers: Headers? = null
    var serverKey: String? = null

    init {
        this.context = context
        this.headers = headers
        this.serverKey = serverPublicKey
        if (!ValuePrefs(context).isHasValues()) {
            ECDHAlgorithm().generateKeys(context)
        }
    }


    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        try {
            val customHeaders = headers ?: headersOf()
            val newRequest: Request
            var strRequestBody = ""
            val mediaType = "application/json; charset=UTF-8".toMediaTypeOrNull()
            if (chain.request().method.compareTo("post", ignoreCase = true) == 0) {
                val oldRequestBody = chain.request().body
                val buffer = Buffer()
                oldRequestBody?.writeTo(buffer)
                val strRequestBuffer = buffer.readUtf8()
                buffer.clear()
                buffer.close()
                val encryptBody = context?.let {
                    ECDHAlgorithm().encryptData(it,serverKey.toString(),strRequestBuffer)
                }
                val m2PCryptPayload = M2PCryptPayload(encryptedReq = encryptBody)
                val newReqString = Gson().toJson(m2PCryptPayload)
                val newReqBody = newReqString.toRequestBody(mediaType)
                strRequestBody = newReqString
                newRequest = chain.request().newBuilder().headers(customHeaders).method(chain.request().method,newReqBody).build()
            }
            else{
                newRequest = chain.request().newBuilder().headers(customHeaders).build()
            }

            val t1 = System.nanoTime()
            createRequestLog(chain,strRequestBody)

            val response = chain.proceed(newRequest)

            val bodyString = responseLogBodyString(response,t1)

            return if (response.code == RESPONSE_UNAUTHORIZED_401) {
                response.body?.close()
                response.newBuilder().code(ResponseCode.RESPONSE_LOGOUT_USER)
                    .body(bodyString.toResponseBody(response.body!!.contentType()))
                    .build()
            } else{
                response.body?.close()
                val responseModel = Gson().fromJson(bodyString,M2PCryptPayload::class.java)
                val decryptRes =
                    context?.let {
                        ECDHAlgorithm().decryptData(serverKey.toString(),responseModel.encryptedRes.toString(),
                            it
                        )
                    }
                val newResBody = decryptRes?.toResponseBody(mediaType)
                response.newBuilder()
                    .body(bodyString.toResponseBody(newResBody?.contentType()))
                    .build()

            }

        } catch (exception: SocketTimeoutException) {
            exception.printStackTrace()
        }
        return chain.proceed(chain.request())
    }

    private fun responseLogBodyString(response: Response, t1: Long):String {
        val t2 = System.nanoTime()
        val responseLog = String.format(
            Locale.US, "Received response for %s in %.1fms%n%s%nResponse Code: %d",
            response.request.url, (t2 - t1) / 1e6, response.headers, response.code
        )
        val bodyString = response.body!!.string()
        Log.d("NetworkInterceptor","response\n$responseLog\n${bodyString}")
        return bodyString
    }

    private fun createRequestLog(chain: Interceptor.Chain, strRequestBody: String) {
        try {
            var requestLog = String.format(
                "Sending request %s on %s%n%s",
                chain.request().url, chain.connection(), chain.request().headers
            )
            if (chain.request().method.compareTo("post", ignoreCase = true) == 0) {
                requestLog =
                    """$requestLog
                    ${strRequestBody}
                    """.trimIndent()
            }
            Log.d("NetworkInterceptor","Network request\n$requestLog")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    object ResponseCode {
        const val RESPONSE_LOGOUT_USER = 9999
    }


}

