package com.m2p.m2pecdhsampleapp


import com.m2p.m2pecdhlib.NetworkInterceptor
import okhttp3.Headers
import okhttp3.OkHttpClient

/**
 * this class show how to build the httpclient for the network calls
 * you can add this ${okHttpClient} on your retrofit builder
 */
class M2PSampleNetworkClass {
    val yourserverkey = ""
    //NetworkInterceptor(yourcontext,yourheaders,yourserverpublic)
    init {
        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(NetworkInterceptor(MainApplication.applicationContext(), Headers.headersOf("Headers"),yourserverkey))
            .build()
    }
}