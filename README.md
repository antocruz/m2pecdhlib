**How this encryption works?**

Alice generates a random ECC key pair: {alicePrivKey, alicePubKey = alicePrivKey * G}

Bob generates a random ECC key pair: {bobPrivKey, bobPubKey = bobPrivKey * G}

Alice and Bob exchange their public keys through the insecure channel (e.g. over Internet)

Alice calculates sharedKey = bobPubKey * alicePrivKey

Bob calculates sharedKey = alicePubKey * bobPrivKey

Now both Alice and Bob have the same sharedKey == bobPubKey * alicePrivKey == alicePubKey * bobPrivKey

---


## Usage of this library

Add this library in your project
You can add the okhttpclient as follows

//code snippet

 val okHttpClient
 = OkHttpClient.Builder()
 .addInterceptor(NetworkInterceptor(MainApplication.applicationContext(), Headers.headersOf("Headers"),yourserverkey))
 .build()

you can add this okHttpClient in your retrofit builder

In NetworkInterceptor() function you have to sent three params
1.your context
2.your headers
3.your serverpublickey

NetworkInterceptor was automatically handle the encryption and decryption for your request and response

---

## Holding public and private key

Generate key will call at the networkinterceptor initialization

and store the public and private key in pref the keys are "Primary_key", "Exposed_key"

if you want the public key you can get by

ValuePrefs(context).getExposedKey()

ValuePrefs was the class file holds both the values

Right now valuepref was plain in future we can add securepref on this to make less vulnerable

---


